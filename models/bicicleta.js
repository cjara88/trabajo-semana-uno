var Bicicleta = function(id, color, modelo, ubicacion) {
    this.id = id;
    this.color  = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;

}

Bicicleta.prototype.toString = function() {
    return 'id: ' + this.id + " | color: " + this.color
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici) {
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId) {
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici) {
        return aBici;
    }else {
        throw new Error(`No existe una bicileta con el id ${aBiciId}`);
    }
}

var a = new Bicicleta(1,'rojo', 'urbana', [-25.2748915,-57.49397]);
var b = new Bicicleta(2, 'blanca', 'urbana',[-25.2762304,-57.4936696]);

Bicicleta.add(a);

Bicicleta.add(b);

Bicicleta.removeId = function(aBiciId) {
    Bicicleta.findById(aBiciId);
    for (let index = 0; index < Bicicleta.allBicis.length; index++) {
       if (Bicicleta.allBicis[index].id == aBiciId) {
        Bicicleta.allBicis.splice(index, 1);
        break;
       }
    }
}

module.exports = Bicicleta;
