var map = L.map('mapid', {
    center: [-25.2760461,-57.5005789,15],
    zoom: 13
});

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
}).addTo(map)

//var marker = L.marker([-25.2743149,-57.4948531,17.25]).addTo(map);

$.ajax({
    dataType: 'json',
    url: 'api/bicicletas',
    success: function(result) {
        console.log(result.bicicletas);
        result.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion, {title : bici.id}).addTo(map);
        });
    }
})